var i = 0;

function layCongViec() {
    i++;
    var congViec = document.getElementById("newTask").value;
    var cv = new CongViec(i, congViec);
    return cv;
}


function renderDscv(dscv) {
    var contentHTML = "";
    for (let i = 0; i < dscv.length; i++) {
        var currentCv = dscv[i];
        var contentLi = `<li>${currentCv.name}<span><i class="fa-regular fa-trash-can" onclick="xoaCv(${dscv[i].id})"></i><i class="fa-solid fa-circle-check" onclick="xongCv(${dscv[i]})"></i></span></li>`
        contentHTML += contentLi;
    }
    document.getElementById("todo").innerHTML = contentHTML;
}

function renderDscvDone(dscvDone) {
    var contentHTML = "";
    for (let i = 0; i < dscvDone.length; i++) {
        var currentCv = dscvDone[i];
        var contentLi = `<li>${currentCv.name}<span><i class="fa-regular fa-trash-can" onclick="xoaCv(${dscv[i].id})"></i><i class="fa-solid fa-circle-check"></i></span></li>`
        contentHTML += contentLi;
    }
    document.getElementById("completed").innerHTML = contentHTML;
}